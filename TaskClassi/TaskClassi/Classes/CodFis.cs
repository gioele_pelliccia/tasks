﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskClassi.Classes
{
    class CodFis
    {
        public string Codice { get; set; }
        public string DataScadenza { get; set; }

        public override string ToString()
        {
            return $"{Codice}-Data Scadenza {DataScadenza}";
        }
    }
}
