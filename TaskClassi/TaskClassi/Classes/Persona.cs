﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskClassi.Classes
{
    class Persona
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public CodFis CodiceFis { get; set; }
        public ID CartaIdentita { get; set; }

        public override string ToString()
        {
            return $"Nome {Nome}-Cognome {Cognome}-\n{CodiceFis}\n{CartaIdentita}";
        }
    }
}
