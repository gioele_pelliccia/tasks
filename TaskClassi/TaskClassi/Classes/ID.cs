﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskClassi.Classes
{
    class ID
    {
        public string Codiceid { get; set; }
        public string DataEmis { get; set; }
        public string DataScad { get; set; }
        public string Emissione;

        public string checkEmissione
        {
            get { return Emissione; }
            set {
                if (value.Equals("Comune") || value.Equals("Zecca dello stato"))
                {
                    Emissione = value;
                    
                }
                else
                    Console.WriteLine("Campo non valido");
            }
        }



        public override string ToString()
        {
            return $"{Codiceid}-Data Emissione {DataEmis}-DataScadenza {DataScad}-Emissione {Emissione}\n";
        }
    }
}
