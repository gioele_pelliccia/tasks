﻿using System;
using TaskClassi.Classes;

namespace TaskClassi
{
    class Program
    {
        static void Main(string[] args)
        {

			/**
			 * Creare un sistema in grado di immagazinare i dati relativi ad una persona.
			 * Inoltre, sarà necessario immagazinare, all'interno di una persona, i dati 
			 * relativi a:
			 * - Codice Fiscale
			 * |- CODICE
			 * |_ Data di scadenza
			 * 
			 * - Carta di Identita: 
			 * |- CODICE
			 * |- Data di Emissione
			 * |- Data di Scadenza
			 * |_ Emissione (comune, zecca dello stato)
			 */

			bool flag = true;

			do
			{
				Console.WriteLine("Immettere dati persona\n");

				Console.WriteLine("Immettere nome");
				string nome = Console.ReadLine();
				Console.WriteLine("Immettere cognome");
				string cognome = Console.ReadLine();
				Console.WriteLine("Immettere codice fiscale");
				string codFis = Console.ReadLine();
				Console.WriteLine("Immettere data di scadenza del codice fiscale");
				string scadCodFis = Console.ReadLine();
				Console.WriteLine("Immettere codice id");
				string idCode = Console.ReadLine();
				Console.WriteLine("Immettere data emissione id");
				string dataEmiId = Console.ReadLine();
				Console.WriteLine("Immettere data scadenza id ");
				string dataScadId = Console.ReadLine();
				Console.WriteLine("Immettere emissione id ");
				string emissione = Console.ReadLine();
				CodFis genericoCodFis = new CodFis()
				{
					Codice = codFis,
					DataScadenza= scadCodFis
				};
				ID genericoId = new ID()
				{
					Codiceid = idCode,
					DataEmis = dataEmiId,
					DataScad = dataScadId,
					Emissione = emissione
				};

				Persona generico = new Persona()
				{
					Nome = nome,
					Cognome =cognome,
					CodiceFis=genericoCodFis,
					CartaIdentita=genericoId

				};
				Console.WriteLine(generico);



			} while (flag);


		}
	}
}
