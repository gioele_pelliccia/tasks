﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskStanze.Classes
{
     public class Stanza
    {
        public string Nome { get; set; }

        

        private List<Oggetto> listaOggetti = new List<Oggetto>() ;

        public List<Oggetto> MyProperty
        {
            get { return listaOggetti; }
            set { listaOggetti = value; }
        }
        public Stanza(string varNome)
        {
            Nome = varNome;
        }

        public void stampaElenco()
        {
            foreach(Oggetto item in listaOggetti)
            {
                Console.WriteLine(item);
            }
        }
    }
}
