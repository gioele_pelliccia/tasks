﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskStanze.Classes
{
    public class Oggetto
    {
        public string Nome { get; set; }
        public string Descrizione { get; set; }
        public float Valore { get; set; }

        public Oggetto(string varNome, string varDescrizione, float varValore )
        {
            Nome = varNome;
            Descrizione = varDescrizione;
            Valore = varValore;

        }
        public override string ToString()
        {
            return $"{Nome} \n {Descrizione} \n {Valore} ";
        }
    }
}
