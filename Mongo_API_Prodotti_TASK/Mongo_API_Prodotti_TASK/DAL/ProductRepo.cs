﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mongo_API_Prodotti_TASK.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Mongo_API_Prodotti_TASK.DAL
{
    public class ProductRepo : IRepo<Product>
    {
        private readonly IMongoCollection<Product> products;

        private readonly string strConnect;
        private readonly string strDb;

        public ProductRepo(string strConnection, string strDatabase)
        {
            var client = new MongoClient(strConnection);
            var db = client.GetDatabase(strDatabase);

            products = db.GetCollection<Product>("Products");
            strConnect = strConnection;
            strDb = strDatabase;
        }

        public IEnumerable<Product> GetAll()
        {
            List<Product> elenco = products.Find(FilterDefinition<Product>.Empty).ToList();
            CategoryRepo tempRepo = new CategoryRepo(strConnect, strDb);
            elenco.ForEach(i => i.InfoCategory = tempRepo.GetById(i.Category));
            return elenco;
        }

        public Product GetById(ObjectId varId)
        {
            Product temp = products.Find(i => i.DocumentiId == varId).FirstOrDefault();
            CategoryRepo tempRepo = new CategoryRepo(strConnect, strDb);
            temp.InfoCategory = tempRepo.GetById(temp.Category);
            return temp;
        } 

        public bool Insert(Product t)
        {
            Product temp = products.Find(i => i.DocumentiId == t.DocumentiId).FirstOrDefault();
            if (temp == null)
            {
                CategoryRepo tempRepo = new CategoryRepo(strConnect, strDb);
                Category tempCate = tempRepo.FindByTitle(t.Categoria);

                if (tempCate != null)
                {
                    t.Category = tempCate.DocumentId;
                    products.InsertOne(t);
                    return true;
                }
            }
            else
            {
                temp.Quantita += t.Quantita;
                var result = products.ReplaceOne(i => i.DocumentiId == temp.DocumentiId, temp);
                if (result.IsAcknowledged && result.ModifiedCount > 0)
                    return true;
            }
            return false;
        }

        public bool Delete(ObjectId varId)
        {
            Product temp = products.Find(i => i.DocumentiId == varId).FirstOrDefault();
            if (temp != null)
            {
                var result = products.DeleteOne(i => i.DocumentiId == varId);
                if (result.IsAcknowledged && result.DeletedCount > 0)
                    return true;
            }
            return false;
        }

        public bool Update(Product t)
        {
            Product temp = products.Find(i => i.DocumentiId == t.DocumentiId).FirstOrDefault();
            if (temp != null)
            {
                CategoryRepo tempRepo = new CategoryRepo(strConnect, strDb);

                temp.Nome = t.Nome ?? temp.Nome;
                temp.Descrizione = t.Descrizione ?? temp.Descrizione;
                temp.Prezzo = t.Prezzo > 0 ? t.Prezzo : temp.Prezzo;
                temp.Sku = t.Sku ?? temp.Sku;
                temp.Quantita = t.Quantita > 0 ? t.Quantita : temp.Quantita;
                temp.Category = t.Category.Equals(ObjectId.Empty) ? temp.Category : t.Category;
                temp.Categoria = tempRepo.GetById(temp.Category).Titolo;
                temp.InfoCategory = tempRepo.GetById(temp.Category);
                var result = products.ReplaceOne(i => i.DocumentiId == temp.DocumentiId, temp);
                if (result.IsAcknowledged && result.ModifiedCount > 0)
                    return true;
            }
            return false;
        }

        public Product FindBySku(string sku)
        {
            Product temp = products.Find(i => i.Sku == sku).FirstOrDefault();
            CategoryRepo tempRepo = new CategoryRepo(strConnect, strDb);
            
            Category cat = tempRepo.GetById(temp.Category);
            if (cat != null)
                temp.InfoCategory = cat;
            return temp;
        }
    }
}
