function stampaProd(){
    fetch("https://localhost:44357/api/products")
        .then(risposta => risposta.json())
        .then(risposta =>{
            let content="";
            for(let item of risposta){
                content+=`
                        <tr>
                            <td>${item.nome}</td>
                            <td>${item.descrizione}</td>
                            <td>${item.prezzo}</td>
                            <td>${item.sku}</td>
                            <td>${item.quantita}</td>
                            <td>${item.categoria}</td>
                        </tr>
                        `
            }
            $("#tbody").html(content);
        })
        .catch(err => console.log(err))     
}
function stampaCategorie(){
    fetch("https://localhost:44357/api/categories")
        .then(risposta => risposta.json())
        .then(risposta =>{
            let content="";
            for(let item of risposta){
                content+=`
                        <tr>
                            <td>${item.titolo}</td>
                            <td>${item.scaffale}</td>
                            <td>${item.descrizione}</td>        
                        </tr>
                        `
            }
            $("#tbodycat").html(content);
        })
        .catch(err => console.log(err))     
}
function insert(){
    let prod = {
        nome:$("#inNome").val(),
        descrizione : $("#Descrizione").val(),
        prezzo : parseFloat($("#Prezzo").val()),
        sku : $("#Sku").val(),
        quantita : parseInt($("#Quantita").val()),
        categoria : $("#Categoria").val()
    }
    fetch("https://localhost:44357/api/products/insert",{
        method: "POST",
        body:JSON.stringify(prod),
        headers: {"Content-type": "application/json;"}
    })
    .then(response => response.json()) 
    .then(prod => console.log("Success:" + prod))
    .then(stampaCategorie())
    .catch(err => console.log(err));
}
$(document).ready(function () {
    stampaCategorie()
    stampaProd()
});