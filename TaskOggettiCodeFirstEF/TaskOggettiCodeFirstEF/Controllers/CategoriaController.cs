﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskOggettiCodeFirstEF.Data;
using TaskOggettiCodeFirstEF.Models;

namespace TaskOggettiCodeFirstEF.Controllers
{
    [Route("api/items")]
    [ApiController]
    public class CategoriaController : Controller
    {
        private readonly InterfaceRepo<Categoria> _repo;

        public CategoriaController(InterfaceRepo<Categoria> rep)
        {
            _repo = rep;
        }
        [HttpGet("categoria/lista")]
        public ActionResult<IEnumerable<Categoria>> GetAllCategorie()
        {
            var elenco = _repo.GetAll();
            return Ok(elenco);
        }
        [HttpGet("categoria/{varId}")]
        public ActionResult<Categoria> GetCategoriaById(int varId)
        {
            var item = _repo.GetById(varId);
            return Ok(item);
        }
        [HttpGet("categoria/{varCodice}")]
        public ActionResult<Items> GetCategoriaByCodice(string varCodice)
        {

            var item = _repo.GetByCodice(varCodice);
            return Ok(item);
        }
        [HttpPost("categoria")]
        public ActionResult InsertCategoria(Categoria item)
        {
            if (_repo.Insert(item))
            {
                return Ok("Successo");
            }
            return Ok("Errore");
        }
        [HttpPut("categoria/{varId}")]
        public ActionResult UpdateCategoria(Categoria item, int varId)
        {
            if (_repo.Update(item, varId))
            {
                return Ok("Successo");
            }
            return Ok("Errore");
        }
        [HttpDelete("categoria/{varId}")]
        public ActionResult DeleteIteme(int varId)
        {
            if (_repo.Delete(varId))
            {
                return Ok("Successo");
            }
            return Ok("Errore");
        }
    }
}
