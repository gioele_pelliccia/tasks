﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskOggettiCodeFirstEF.Data;
using TaskOggettiCodeFirstEF.Models;

namespace TaskOggettiCodeFirstEF.Controllers
{
    [Route("api/items")]
    [ApiController]
    public class ItemsController : Controller
    {
        private readonly InterfaceRepo<Items> _repo;

        public ItemsController(InterfaceRepo<Items> rep)
        {
            _repo = rep;
        }
        [HttpGet("lista")]
        public ActionResult<IEnumerable<Items>> GetAllItems()
        {
            
                var elenco = _repo.GetAll();

            
            
            

            return Ok(elenco);
        }
        [HttpGet("{varId}")]
        public ActionResult<Items> GetItemById(int varId)
        {
            
            var item = _repo.GetById(varId);
            return Ok(item);
        }
        [HttpGet("codice/{varCodice}")]
        public ActionResult<Items> GetItemByCodice(string varCodice)
        {

            var item = _repo.GetByCodice(varCodice);
            return Ok(item);
        }
        [HttpPost]
        public ActionResult InsertItem(Items item)
        {
           Items obj = _repo.GetByCodice(item.Codice);
           if (obj==null)
            {
                
                if (_repo.Insert(item))
                {
                    return Ok("Successo");
                }
                return Ok("Errore");
            }
            else
            {
                item.Quantita+=obj.Quantita;
                if(_repo.Update(item, obj.Id))
                {
                    return Ok("Successo");
                }
                return Ok("Errore");
            }
            
        }
        [HttpPut("{varId}")]
        public ActionResult UpdateItem(Items item,int varId)
        {
            if (_repo.Update(item, varId))
            {
                return Ok("Successo");
            }
            return Ok("Errore");
        }
        [HttpDelete("{varId}")]
        public ActionResult DeleteIteme(int varId)
        {
            if (_repo.Delete(varId))
            {
                return Ok("Successo");
            }
            return Ok("Errore");
        }
    }
}
