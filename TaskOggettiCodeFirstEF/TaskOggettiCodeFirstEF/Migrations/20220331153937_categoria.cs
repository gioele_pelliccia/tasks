﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TaskOggettiCodeFirstEF.Migrations
{
    public partial class categoria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CategoriaId",
                table: "Items",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Categoria",
                columns: table => new
                {
                    CategoriaId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Titolo = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Descrizione = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Scaffale = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Codice = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categoria", x => x.CategoriaId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Items_CategoriaId",
                table: "Items",
                column: "CategoriaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Categoria_CategoriaId",
                table: "Items",
                column: "CategoriaId",
                principalTable: "Categoria",
                principalColumn: "CategoriaId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_Categoria_CategoriaId",
                table: "Items");

            migrationBuilder.DropTable(
                name: "Categoria");

            migrationBuilder.DropIndex(
                name: "IX_Items_CategoriaId",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "CategoriaId",
                table: "Items");
        }
    }
}
