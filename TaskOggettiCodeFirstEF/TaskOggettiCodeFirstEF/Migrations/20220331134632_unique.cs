﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TaskOggettiCodeFirstEF.Migrations
{
    public partial class unique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Items_Codice",
                table: "Items",
                column: "Codice",
                unique: true,
                filter: "[Codice] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Items_Codice",
                table: "Items");
        }
    }
}
