﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskOggettiCodeFirstEF.Models;

namespace TaskOggettiCodeFirstEF.Data
{
    public class ItemsRepo : InterfaceRepo<Items>
    {
        private readonly DepositoContext _contesto;
        public ItemsRepo(DepositoContext cont)
        {
            _contesto = cont;
        }

        public bool Delete(int varId)
        {
            _contesto.Items.Remove(GetById(varId));
            if (_contesto.SaveChanges() > 0)
            {
                return true;
            }
            return false;
        }

        public IEnumerable<Items> GetAll()
        {
           return _contesto.Items.ToList();
        }

        public Items GetById(int varId)
        {
           return _contesto.Items.FirstOrDefault(i => i.Id == varId);
        }
        public Items GetByCodice(string varCodice)
        {
            return _contesto.Items.FirstOrDefault(i => i.Codice == varCodice);
        }

        public bool Insert(Items t)
        {
            _contesto.Items.Add(t);

            if (_contesto.SaveChanges() > 0)
            {
                return true;
            }
            return false;
        }

        public bool Update(Items t,int varId)
        {
            var result = _contesto.Items.Find(varId);
            
                
            if (result!=null)
            {
                result.Nome = t.Nome;
                result.Descrizione = t.Descrizione;
                result.Codice = t.Codice;
                result.Quantita = t.Quantita;
                
            }
            if (_contesto.SaveChanges() > 0)
            {
                return true;
            }
            return false;
        }
    }
}
