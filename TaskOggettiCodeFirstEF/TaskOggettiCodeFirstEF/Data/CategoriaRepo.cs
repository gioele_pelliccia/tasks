﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskOggettiCodeFirstEF.Models;

namespace TaskOggettiCodeFirstEF.Data
{
    public class CategoriaRepo : InterfaceRepo<Categoria>
    {
        private readonly DepositoContext _contesto;
        public CategoriaRepo(DepositoContext cont)
        {
            _contesto = cont;
        }
        public bool Delete(int varId)
        {
            _contesto.Categoria.Remove(GetById(varId));
            if (_contesto.SaveChanges() > 0)
            {
                return true;
            }
            return false;
        }

        public IEnumerable<Categoria> GetAll()
        {
            return _contesto.Categoria.ToList();
        }

        public Categoria GetByCodice(string varCodice)
        {
            return _contesto.Categoria.FirstOrDefault(i => i.Codice == varCodice);
        }

        public Categoria GetById(int varId)
        {
            return _contesto.Categoria.FirstOrDefault(i => i.CategoriaId == varId);
        }

        public bool Insert(Categoria t)
        {
            _contesto.Categoria.Add(t);

            if (_contesto.SaveChanges() > 0)
            {
                return true;
            }
            return false;
        }

        public bool Update(Categoria t, int varId)
        {
            var result = _contesto.Categoria.Find(varId);


            if (result != null)
            {
                result.Titolo = t.Titolo;
                result.Descrizione = t.Descrizione;
                result.Codice = t.Codice;
                result.Scaffale = t.Scaffale;

            }
            if (_contesto.SaveChanges() > 0)
            {
                return true;
            }
            return false;
        }
    }
}
