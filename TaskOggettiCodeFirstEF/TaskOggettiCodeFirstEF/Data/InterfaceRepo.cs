﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskOggettiCodeFirstEF.Data
{
    
    
        public interface InterfaceRepo<T>
        {
            IEnumerable<T> GetAll();
            T GetById(int varId);
            bool Delete(int varId);
            bool Update(T t, int varId);
            bool Insert(T t);
            T GetByCodice(string varCodice);

    }
    
}
