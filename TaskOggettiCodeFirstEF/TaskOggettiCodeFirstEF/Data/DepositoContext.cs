﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskOggettiCodeFirstEF.Models;

namespace TaskOggettiCodeFirstEF.Data
{
    public class DepositoContext : DbContext
    {
        public DepositoContext(DbContextOptions<DepositoContext> opt) : base(opt)
        {

        }

        public DbSet<Items> Items { get; set; }
        public DbSet<Categoria> Categoria { get; set; }
        public IEnumerable<object> ItemEnum { get; internal set; }
    }
}

