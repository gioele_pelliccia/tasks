﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace TaskOggettiCodeFirstEF.Models
{
    public class Categoria
    {
        //Ogni categoria è caratterizzata da:
        //- Titolo
        //- Scaffale
        //- Descrizione
        //- Codice univoco(ASSEGNATO AUTOMATICAMENTE)
        [Key]
        public int CategoriaId { get; set; }
        [MaxLength(250)]
        public string Titolo { get; set; }
        [MaxLength(250)]
        public string Descrizione { get; set; }
        [MaxLength(250)]
        public string Scaffale { get; set; }
        [MaxLength(250)]
        [Required]
       // [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string Codice { get; set; }

        public Categoria ()
        {
            Codice = MD5.Create
                (new Random().Next(1000000).ToString()).ToString();

        }
    }

}
