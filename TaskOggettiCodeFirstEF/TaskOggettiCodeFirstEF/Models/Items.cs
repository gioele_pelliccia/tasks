﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TaskOggettiCodeFirstEF.Models
{
   [Index(nameof(Codice), IsUnique = true)]
    public class Items
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(250)]
        public string Nome { get; set; }
       // [Column(TypeName = "TEXT")]
        public string Descrizione { get; set; }
        [MaxLength(250)]
        
        public string Codice { get; set; }
        [Required]
        public int Quantita { get; set; }
        
        [Required]
        public Categoria Categoria { get; set; }
        

    }
}
