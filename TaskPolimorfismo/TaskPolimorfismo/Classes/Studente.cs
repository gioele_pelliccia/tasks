﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskPolimorfismo.Classes
{
    class Studente : Persona
    {
        private static int contaStudenti = 0; //Contatore ausiliario

        

        public static int ContaStudenti
        {
            get { return contaStudenti; }
           // set { contaStudenti = value; }              Non c e' bisogno del set il valore non vuole essere cambiato
        }

        public string CorsoStudi { get; set; }
        private  int Matricola { get; set; }

        


            public Studente(
                string varNome,
                string varCognome,
                string CorsoStudi
                )
        {
            Nome = varNome;
            Cognome = varCognome;
            contaStudenti++;                                                //Se dichiarassi la matricola statica verrebbe inizializzata
                                                                            //    prima delle istanze di classe
            Matricola = contaStudenti;
            this.CorsoStudi = CorsoStudi;
        }
        public override string ToString()
        {
            return $"Studente{Nome},{Cognome} - Matricola: {Matricola}";
        }

    }
}
