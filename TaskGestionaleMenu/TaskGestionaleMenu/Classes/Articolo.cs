﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskGestionaleMenu.Classes
{
    public abstract class Articolo
    {
        public string Codice { get; set; }
        public string Nome { get; set; }
        public double Prezzo { get; set; }

        public Articolo()
        {

        }
        public override string ToString()
        {
            return $"Codice - {Codice} -Nome {Nome} Prezzo {Prezzo}";
        }
    }
}
