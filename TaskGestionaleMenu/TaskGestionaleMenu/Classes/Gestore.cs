﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskGestionaleMenu.Classes
{
    class Gestore
    {
        private List<Bevanda> listaBevande = new List<Bevanda>();

        public List<Bevanda> ListaBevande 
        {
            get { return listaBevande; }
            set { listaBevande = value; }
        }
        private List<Piatto> listaPiatti = new List<Piatto>();

        public List<Piatto> ListaPiatti
        {
            get { return listaPiatti; }
            set { listaPiatti = value; }
        }
        public Gestore()
        {
            listaBevande.Add(new Bevanda("123123", "Sprite", 1.35d));
            listaBevande.Add(new Bevanda("123234", "Coca", 1.50d));
            listaPiatti.Add(new Piatto("123567", "Filetto alla Wellington", 40.50d));
            listaPiatti.Add(new Piatto("123987", "Trota", 30.50d));
        }
        //fare trycatch sulle aggiunte
        public bool aggiungiBevanda(string varCodice, string varNome, double varPrezzo)
        {
            Bevanda tempBevanda = new Bevanda(varCodice, varNome, varPrezzo);
            listaBevande.Add(tempBevanda);
            return true;
        }
        public bool aggiungiPiatto(string varCodice, string varNome, double varPrezzo)
        {
            Bevanda tempPiatto = new Bevanda(varCodice, varNome, varPrezzo);
            listaBevande.Add(tempPiatto);
            return true;
        }
        public void printListaBevande()
        {
            foreach(Bevanda bev in listaBevande)
            {
                Console.WriteLine(bev);
            }
        }
        public void printListaPiatti()
        {
            foreach (Articolo piatto in listaPiatti)
            {
                Console.WriteLine(piatto);
            }
        }
        public int contaPiatti()
        {
            int contatore = 0;
            foreach (Piatto piatto in listaPiatti)
                contatore++;
            return contatore;
            
        }
        public int contaBevande()
        {
            int contatore = 0;
            foreach (Bevanda bev in listaBevande)
                contatore++;
            return contatore;

        }
    }
}
