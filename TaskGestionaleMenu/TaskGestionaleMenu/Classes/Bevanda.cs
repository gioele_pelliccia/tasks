﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskGestionaleMenu.Classes
{
    public class Bevanda : Articolo
    {
        public Bevanda()
        {

        }
        public Bevanda(string varCodice, string varNome, double varPrezzo)
        {
            Nome = varNome;
            Codice = varCodice;
            Prezzo = varPrezzo;

        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
