﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskGestionaleMenu.Classes
{
    public class Piatto : Articolo
    {
        public Piatto()
        {

        }
        public Piatto(string varCodice, string varNome, double varPrezzo) 
        {
            Nome = varNome;
            Codice = varCodice;
            Prezzo = varPrezzo;

        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
