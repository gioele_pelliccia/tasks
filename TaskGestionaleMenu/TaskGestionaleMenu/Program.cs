﻿using System;
using TaskGestionaleMenu.Classes;

namespace TaskGestionaleMenu
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Creare un software per la gestione del menu di UN ristorante,
            * questo avrà due categorie di articolo: bevande, piatti.

            * Creare i metodi in grado di:
            * -Inserire una bevanda o un piatto
            * - Stampare tutte le bevande o piatti(a scelta)
            * ->Contare tutte le bevande o piatti(a scelta)

            * Ogni articolo avrà almeno:
            * -Codice
            * - Nome
            * Prezzo
            * 
            */


            Gestore gestore = new Gestore();

            Console.WriteLine(gestore.contaBevande());
            Console.WriteLine(gestore.contaPiatti());
            gestore.printListaBevande();
            gestore.printListaPiatti();
            string varCodice;
            string varNome;
            double varPrezzo;
            
            
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("\n1Inserire una bevanda\n2 Inserire un piatto\n3Stampare bevande\n4Stampare piatti\n5Contare Bevande\n6Contare piatti\n7Uscire");
                string input = Console.ReadLine();
                switch (input)
                {
                    case  "1":
                        varCodice = Console.ReadLine();
                        varNome = Console.ReadLine();
                        varPrezzo = Convert.ToDouble(Console.ReadLine());
                        if (gestore.aggiungiBevanda(varNome, varCodice, varPrezzo))
                        {
                            Console.WriteLine("Bevanda Aggiunta!");
                        }else
                            Console.WriteLine("Errore, Controlla i dati inseriti");
                        break;
                    case "2":
                        varCodice = Console.ReadLine();
                        varNome = Console.ReadLine();
                        varPrezzo = Convert.ToDouble(Console.ReadLine());
                        if (gestore.aggiungiPiatto(varNome, varCodice, varPrezzo))
                        {
                            Console.WriteLine("Piatto Aggiunta!");
                        }
                        else
                            Console.WriteLine("Errore, Controlla i dati inseriti");
                        break;
                    case "3":
                        gestore.printListaBevande();
                        break;
                    case "4":
                        gestore.printListaPiatti();
                        break;
                    case "5":
                        gestore.contaBevande();
                        break;
                    case "6":
                        gestore.contaPiatti();
                        break;
                    case "7":
                        flag = !flag;
                        break;


                }

            }
        }
    }
}
