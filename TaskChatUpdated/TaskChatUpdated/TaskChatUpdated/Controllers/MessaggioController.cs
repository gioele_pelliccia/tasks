﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskChatUpdated.Data;
using TaskChatUpdated.Models;

namespace TaskChatUpdated.Controllers
{
    [Route("api/chat")]
    [ApiController]
    public class MessaggioController : Controller
    {
        private readonly InterfaceRepo<Messaggio> _repo;
        public MessaggioController(InterfaceRepo<Messaggio> rep)
        {
            _repo = rep;
        }
        [HttpGet]
        public ActionResult<IEnumerable<Messaggio>> GetAllMessaggi()
        {
            return Ok (_repo.GetAll());
        }
        [HttpPost("insert")]
        public ActionResult InsertMessaggio(Messaggio mess)
        {
            try
            {
                _repo.Insert(mess);
                return Ok("success");
            }catch(Exception ex)
            {
                return Ok("error");
            }
            
        }
    }
}
