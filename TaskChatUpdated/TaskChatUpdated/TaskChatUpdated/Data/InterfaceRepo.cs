﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskChatUpdated.Data
{
    public interface InterfaceRepo<T>
    {
        public bool Insert(T t);
        public IEnumerable<T> GetAll();
    }
}
