﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskChatUpdated.Models;

namespace TaskChatUpdated.Data
{
    public class MessaggioRepo : InterfaceRepo<Messaggio>
    {
        private readonly TaskChatUpdatedContext _con;
        public MessaggioRepo(TaskChatUpdatedContext con)
        {
            _con = con;
        }
        public bool Insert(Messaggio mess)
        {
            try
            {
                _con.Messaggios.Add(mess);
                _con.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public IEnumerable<Messaggio> GetAll()
        {
            return _con.Messaggios.ToList();
        }
    }
}
