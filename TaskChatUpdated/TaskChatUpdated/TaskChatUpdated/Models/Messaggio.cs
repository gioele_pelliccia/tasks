﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TaskChatUpdated.Models
{
    public partial class Messaggio
    {
        public int MessaggioId { get; set; }
        public string Username { get; set; }
        public string Descrizione { get; set; }
        public DateTime? Orario { get; set; }
    }
}
