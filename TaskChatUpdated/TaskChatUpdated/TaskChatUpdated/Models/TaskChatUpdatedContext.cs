﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace TaskChatUpdated.Models
{
    public partial class TaskChatUpdatedContext : DbContext
    {
        public TaskChatUpdatedContext()
        {
        }

        public TaskChatUpdatedContext(DbContextOptions<TaskChatUpdatedContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Messaggio> Messaggios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-VULS57I\\SQLEXPRESS;Database=TaskChatUpdated;User Id=sharpuser;Password=cicciopasticcio;Trusted_Connection=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<Messaggio>(entity =>
            {
                entity.ToTable("Messaggio");

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.Orario)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
