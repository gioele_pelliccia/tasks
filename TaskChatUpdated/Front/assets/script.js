// if(localStorage.getItem("username") == null)
//     localStorage.setItem("username", JSON.stringify([]));

// let username = JSON.parse(localStorage.getItem("username"));

// stampaChat();
function LogIn() {
  let username = $("#inUserName").val();
  localStorage.setItem("username", JSON.stringify(username));
  console.log(username);
}
function stampaChat() {
  $.ajax({
    url: "https://localhost:44311/api/chat",
    method: "GET",
    success: function (response) {
      let content = "";
      for (let item of response) {
        content += `
                            <li style="list-style-type: none">
                                <p class="">
                                    
                                    <strong>${item.username}</strong>
                                    <small>${item.orario}</small>
                                </p> 
                                <p>${item.descrizione}</p>
                            </li>
                            <hr />
                `;
      }
      $("#chat").html(content);
    },
  });
}
function inviaMessaggio() {
  let messaggio = {
    username: JSON.parse(localStorage.getItem("username")),
    descrizione: $("#messaggio").val(),
  };
  // console.log(messaggio);
  $.ajax({
    method: "POST",
    url: "https://localhost:44311/api/chat/insert",
    data: JSON.stringify(messaggio),
    dataType: "json",
    contentType: "application/json",
    success: function (response) {
      switch (response.result) {
        case "success":
          $("#messaggio").val("");
        case "error":
          alert("errore nell'invio");
      }
    },
  });
}
$(document).ready(function () {
  setInterval(() => {
    stampaChat();
  }, 100);
  $("#LogIn").click(LogIn);

  $("#LogIn").click(() => {
    window.location.href = "/Front/chat.html";
  });
});
