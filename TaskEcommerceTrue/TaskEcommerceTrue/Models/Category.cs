﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaskEcommerceTrue.Models
{
    public class Category
    {
        [BsonId]
        public ObjectId DocumentID { get; set; }
        [Required]
        [MaxLength(150)]
        public string Nome { get; set; }
        [Required]
        [MaxLength(150)]
        public string Descrizione { get; set; }
        [Required]
        [MaxLength(150)]
        public string Scaffale { get; set; }
        [MaxLength(150)]
        public string Codice { get; set; }
    }
}
