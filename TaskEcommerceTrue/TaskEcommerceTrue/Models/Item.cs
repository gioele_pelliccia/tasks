﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaskEcommerceTrue.Models
{
    public class Item
    {
        [BsonId]
        public ObjectId DocumentID { get; set; }
        [Required]
        [MaxLength(250)]
        public string Nome { get; set; }
        [Required]
        [MaxLength(250)]
        public string Descrizione { get; set; }
        [Required]
        [MaxLength(250)]
        public string Categoria { get; set; }
        [Required]
        
        public int Quantita { get; set; }
        [Required]
        [MaxLength(250)]
        public string CodiceSKU { get; set; }
        public ObjectId Category { get; set; }
        [BsonIgnore]
        public Category InfoCategoria { get; set; }
    }
}
