﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskEcommerceTrue.Models;

namespace TaskEcommerceTrue.DAL
{
    public class CategoryRepo : InterfaceRepo<Category>
    {
        private IMongoCollection<Category> categorie;
        public CategoryRepo(string strConnessione,string strDatabase)
        {
            var client = new MongoClient(strConnessione);
            var db = client.GetDatabase(strDatabase);

            if (categorie == null)
                categorie = db.GetCollection<Category>("Categories");
        }
        public bool Delete(ObjectId varId)
        {
            Category temp = GetById(varId);
            if (temp != null)
            {
                var result = categorie.DeleteOne<Category>(e => e.DocumentID == varId);

                if (result.IsAcknowledged && result.DeletedCount > 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public IEnumerable<Category> GetAll()
        {
            return categorie.Find(FilterDefinition<Category>.Empty).ToList();
        }

        public Category GetById(ObjectId varId)
        {
            return categorie.Find(d => d.DocumentID == varId).FirstOrDefault();
        }

        public bool Insert(Category t)
        {
            Category temp = categorie.Find(d => d.Codice == t.Codice).FirstOrDefault();
            if (temp == null)
            {
                categorie.InsertOne(t);
                if (t.DocumentID != null)
                    return true;
            }
            return false;
        }

        public bool Update(Category t)
        {
            Category temp = categorie.Find(c => c.Codice == t.Codice).FirstOrDefault();
            if (temp != null)
            {
                temp.Nome = t.Nome != null ? t.Nome : temp.Nome;
                temp.Descrizione = t.Descrizione != null ? t.Descrizione : temp.Descrizione;
                temp.Scaffale = t.Scaffale != null ? t.Scaffale : temp.Scaffale;

                var filter = Builders<Category>.Filter.Eq(c => c.DocumentID, t.DocumentID);
                var result = categorie.ReplaceOne(filter, temp);

                if (result.IsAcknowledged && result.ModifiedCount > 0)
                    return true;
                else
                    return false;
                
            }
            else
            {
                return false;
            }
            

            
        }
    }
}
