﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskEcommerceTrue.DAL
{
    interface InterfaceRepo<T>
    {
        IEnumerable<T> GetAll();
        T GetById(ObjectId varId);
        bool Insert(T t);
        bool Delete(ObjectId varId);
        bool Update(T t);
    }
}
