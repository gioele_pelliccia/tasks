﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskEcommerceTrue.DAL;
using TaskEcommerceTrue.Models;

namespace TaskEcommerceTrue.Controllers
{
    [ApiController]
    [Route("api/categorie")]
    public class CategoryController : Controller
    {
        private readonly CategoryRepo _repository;

        public CategoryController(IConfiguration configurazione)
        {
            bool isLocale = configurazione.GetValue<bool>("IsLocale");

            string stringaConnessione = isLocale == true ?
                configurazione.GetValue<string>("MongoSettings:DatabaseLocale") :
                configurazione.GetValue<string>("MongoSettings:DatabaseRemoto");
            string nomeDatabase = configurazione.GetValue<string>("MongoSettings:NomeDatabase");

            _repository = new CategoryRepo(stringaConnessione, nomeDatabase);
        }
        [HttpPost("inserisci")]
        public ActionResult Inserisci(Category cat)
        {
            cat.Codice = Guid.NewGuid().ToString().Substring(0, 10);
            if (_repository.Insert(cat))
            {
                return Ok(new { Status = "success", Descrizione = "" });
            }
            return Ok(new { Status = "errore", Descrizione = "Inserimento Fallito!" });
        }
        [HttpGet]
        public ActionResult<IEnumerable<Category>> Lista()
        {
            return Ok(_repository.GetAll());
        }
        [HttpGet("{varId}")]
        public ActionResult<IEnumerable<Category>> OttieniId(ObjectId varId)
        {
            return Ok(_repository.GetById(varId));
        }
        [HttpDelete]
        public ActionResult Elimina(ObjectId objectId)
        {
            if(_repository.Delete(objectId))
                return Ok(new { Status = "success", Descrizione = "" });

            return Ok(new { Status = "errore", Descrizione = "Eliminazione Fallito!" });
        }
        [HttpPut("{varId}")]
        public ActionResult Modifica(ObjectId varId,Category t)
        {
            t.Codice = varId.ToString();
            if (_repository.Update(t))
                return Ok(new { Status = "success" });
            else
                return Ok(new { Status = "error" });
        }    
        

        
    }
}
