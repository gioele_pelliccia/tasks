﻿using CorsoUtente_TASK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorsoUtente_TASK.Data
{
    public class IscrizioneRepo :InterfaceRepo<Iscrizione>
    {
        private readonly PalestraTaskContext _context;
        public IscrizioneRepo(PalestraTaskContext con)
        {
            _context = con;
        }

        public bool Delete(int varId)
        {
            try
            {
                Iscrizione t = GetById(varId);
                _context.Iscriziones.Remove(t);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Iscrizione> GetAll()
        {
           List<Iscrizione> lista =_context.Iscriziones.ToList();
            foreach(Iscrizione item in lista)
            {
                item.CorsoRifNavigation = _context.Corsos.Find(item.CorsoRif);
                item.UtenteRifNavigation = _context.Utentes.Find(item.UtenteRif);
            }
            return lista;
        }

        public Iscrizione GetById(int varId)
        {
            return _context.Iscriziones.Find(varId);
        }

        public Iscrizione GetFromCodice(string codice)
        {
            throw new NotImplementedException();
        }

        public Iscrizione GetFromUsername(string codice)
        {
            throw new NotImplementedException();
        }

        public Iscrizione Insert(Iscrizione t)
        {
            _context.Iscriziones.Add(t);
            _context.SaveChanges();
            return t;
        }

        public bool Update(Iscrizione t)
        {
            throw new NotImplementedException();
        }
    }
}
