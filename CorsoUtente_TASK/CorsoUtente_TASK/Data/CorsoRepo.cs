﻿using CorsoUtente_TASK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorsoUtente_TASK.Data
{
    public class CorsoRepo : InterfaceRepo<Corso>
    {
        private readonly PalestraTaskContext _context;
        public CorsoRepo(PalestraTaskContext con)
        {
            _context = con;
        }

        public bool Delete(int varId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Corso> GetAll()
        {
            return _context.Corsos.ToList();
        }

        public Corso GetById(int varId)
        {
            return _context.Corsos.Find(varId);
        }

        public Corso GetFromCodice(string codice)
        {
            throw new NotImplementedException();
        }

        public Corso GetFromUsername(string codice)
        {
            throw new NotImplementedException();
        }

        public Corso Insert(Corso t)
        {
            throw new NotImplementedException();
        }

        public bool Update(Corso t)
        {
            throw new NotImplementedException();
        }
    }
}
