﻿using CorsoUtente_TASK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorsoUtente_TASK.Data
{
    public class UtenteRepo : InterfaceRepo<Utente>
    {
        private readonly PalestraTaskContext _context;
        public UtenteRepo(PalestraTaskContext con)
        {
            _context = con;
        }

        public bool Delete(int varId)
        {
            try
            {
                Utente t = GetById(varId);
                _context.Utentes.Remove(t);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Utente> GetAll()
        {
            return _context.Utentes.ToList();
        }

        public Utente GetById(int varId)
        {
            return _context.Utentes.Find(varId);
        }

        public Utente GetFromCodice(string codice)
        {
            throw new NotImplementedException();
        }

        public Utente GetFromUsername(string Username)
        {
            
            return _context.Utentes.Where(o => o.Username == Username).FirstOrDefault();
        }

       

        public Utente Insert(Utente t)
        {
            _context.Utentes.Add(t);
            _context.SaveChanges();
            return t;
        }

        public bool Update(Utente t)
        {
            throw new NotImplementedException();
        }
    }
}
