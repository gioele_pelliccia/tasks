﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace CorsoUtente_TASK.Models
{
    public partial class Utente
    {
        public Utente()
        {
            Iscriziones = new HashSet<Iscrizione>();
        }
        
        public int UtenteId { get; set; }
        [Required(ErrorMessage ="Il campo non e' presente o valido")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Il campo non e' presente o valido")]
        public string Cognome { get; set; }
        [Required(ErrorMessage = "Il campo non e' presente o valido")]
        public string Indirizzo { get; set; }
        [Required(ErrorMessage = "Il campo non e' presente o valido")]
        public DateTime? DataNascita { get; set; }
        [Required(ErrorMessage = "Il campo non e' presente o valido")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Il campo non e' presente o valido")]
        public string PasswordUtente { get; set; }
        

        public virtual ICollection<Iscrizione> Iscriziones { get; set; }
    }
}
