﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CorsoUtente_TASK.Models
{
    public partial class Iscrizione
    {
        public int IscrizioneId { get; set; }
        public DateTime DataIscrizione { get; set; }
        public int? CorsoRif { get; set; }
        public int? UtenteRif { get; set; }

        public virtual Corso CorsoRifNavigation { get; set; }
        public virtual Utente UtenteRifNavigation { get; set; }
    }
}
