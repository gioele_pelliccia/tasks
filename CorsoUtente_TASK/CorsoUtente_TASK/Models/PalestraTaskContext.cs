﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace CorsoUtente_TASK.Models
{
    public partial class PalestraTaskContext : DbContext
    {
        public PalestraTaskContext()
        {
        }

        public PalestraTaskContext(DbContextOptions<PalestraTaskContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Corso> Corsos { get; set; }
        public virtual DbSet<Iscrizione> Iscriziones { get; set; }
        public virtual DbSet<Utente> Utentes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-VULS57I\\SQLEXPRESS;Database=PalestraTask;User Id=sharpuser;Password=cicciopasticcio;Trusted_Connection=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<Corso>(entity =>
            {
                entity.ToTable("Corso");

                entity.HasIndex(e => e.Codice, "UQ__Corso__0636EC1D437BBBD8")
                    .IsUnique();

                entity.Property(e => e.Codice)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DataOra).HasColumnType("smalldatetime");

                entity.Property(e => e.Descrizione)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Titolo)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Iscrizione>(entity =>
            {
                entity.ToTable("Iscrizione");

                entity.Property(e => e.CorsoRif).HasColumnName("CorsoRIF");

                entity.Property(e => e.DataIscrizione)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UtenteRif).HasColumnName("UtenteRIF");

                entity.HasOne(d => d.CorsoRifNavigation)
                    .WithMany(p => p.Iscriziones)
                    .HasForeignKey(d => d.CorsoRif)
                    .HasConstraintName("FK__Iscrizion__Corso__2A4B4B5E");

                entity.HasOne(d => d.UtenteRifNavigation)
                    .WithMany(p => p.Iscriziones)
                    .HasForeignKey(d => d.UtenteRif)
                    .HasConstraintName("FK__Iscrizion__Utent__2B3F6F97");
            });

            modelBuilder.Entity<Utente>(entity =>
            {
                entity.ToTable("Utente");

                entity.Property(e => e.Cognome)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DataNascita).HasColumnType("smalldatetime");

                entity.Property(e => e.Indirizzo)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordUtente)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
