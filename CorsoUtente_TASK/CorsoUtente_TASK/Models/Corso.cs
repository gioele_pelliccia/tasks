﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CorsoUtente_TASK.Models
{
    public partial class Corso
    {
        public Corso()
        {
            Iscriziones = new HashSet<Iscrizione>();
        }

        public int CorsoId { get; set; }
        public string Titolo { get; set; }
        public string Descrizione { get; set; }
        public string Codice { get; set; }
        public DateTime? DataOra { get; set; }

        public virtual ICollection<Iscrizione> Iscriziones { get; set; }
    }
}
