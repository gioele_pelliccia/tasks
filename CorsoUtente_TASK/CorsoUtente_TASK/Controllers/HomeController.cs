﻿using CorsoUtente_TASK.Data;
using CorsoUtente_TASK.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorsoUtente_TASK.Controllers
{
    public class HomeController : Controller
    {
        private readonly InterfaceRepo<Utente> _repositoryUt;
        private readonly InterfaceRepo<Iscrizione> _repositoryIsc;
        private readonly InterfaceRepo<Corso> _repositoryCor;

        public HomeController(InterfaceRepo<Utente> repu, InterfaceRepo<Iscrizione> repi, InterfaceRepo<Corso> repc)
        {
            _repositoryUt = repu;
            _repositoryIsc = repi;
            _repositoryCor = repc;
        }

        public IActionResult Index()
        {
           List<Corso> listac = (List<Corso>)_repositoryCor.GetAll();
            ViewBag.Title = "Lista Corsi";
            
            
            return View(listac);
        }
        public IActionResult Insert()
        {
            

            return View();
        }
        [HttpPost]
        public IActionResult Insert(Utente ut)
        {
            if (ModelState.IsValid)
            {
                _repositoryUt.Insert(ut);
                return Redirect("/Home/Index");
            }
            else
            {

                return View(ut);
            }
        }
        [HttpGet]
        public IActionResult IMieiCorsi()
        {
            string connection = HttpContext.Session.GetString("IsLogged");
                if (connection == null)
                {
                    return Redirect("/Home/LogIn");
                }
             Utente ut = _repositoryUt.GetFromUsername(connection);
            
                
            List<Iscrizione> listaI = (List<Iscrizione>)_repositoryIsc.GetAll().Where(i=>i.UtenteRif==ut.UtenteId).ToList();
            ViewBag.Title = "Le mie Iscrizioni";
            
                
            return View(listaI);
        }
        
        public IActionResult LogIn()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CheckLogIn(Utente ut)
        {
            Utente temp = _repositoryUt.GetFromUsername(ut.Username);
            if (temp.Username != null && temp.PasswordUtente==ut.PasswordUtente)
            {
                HttpContext.Session.SetString("IsLogged", temp.Username);
                

                return Redirect("/Home/Index");
            }
            else
            {
                return Redirect("/Home/Login");
            }

        }
        public bool IsLogged()
        {
            
            if (HttpContext.Session.GetString("IsLogged") != null)
                return true;
            else
                return false;
        }
        [HttpPost]
        public IActionResult Iscriviti(int id)
        {
            if (IsLogged())
            {
                string str = HttpContext.Session.GetString("IsLogged");
                Utente temp = _repositoryUt.GetFromUsername(str); 
                Corso cor = _repositoryCor.GetById(id);
                
                Iscrizione isc = new Iscrizione()
                {
                    CorsoRif = id,
                    UtenteRif = temp.UtenteId,
                    CorsoRifNavigation = cor,
                    UtenteRifNavigation = temp
                };

                if (_repositoryIsc.GetAll().Where(i => i.CorsoRif == cor.CorsoId && i.UtenteRif==temp.UtenteId).FirstOrDefault() == null)
                {
                    _repositoryIsc.Insert(isc);
                    temp.Iscriziones.Add(isc);
                    cor.Iscriziones.Add(isc);
                    return Redirect("/Home/Index");
                }
                else
                    return Redirect("/Home/Index");


            }
            else
                return Redirect("/Home/LogIn");


        }
        
        public IActionResult DeleteIscrizione(int id)
        {
            _repositoryIsc.Delete(id);
           
            return Redirect("/Home/IMieiCorsi");
        }
    }
}
