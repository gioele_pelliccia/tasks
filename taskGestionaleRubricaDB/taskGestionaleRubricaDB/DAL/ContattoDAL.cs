﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using taskGestionaleRubricaDB.Models;

namespace taskGestionaleRubricaDB.DAL
{
    public class ContattoDAL
    {
        private string stringaConnessione;      //Si puo mettere static per non ripetere l operazione con un if(stringaConnessione==null)
        public ContattoDAL(IConfiguration config)
        {
            stringaConnessione = config.GetConnectionString("ServerLocale");
        }

        public bool InsertContatto(Contatto objContatto)
        {

            using(SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "INSERT INTO Contatto (nome,cognome,numeroTel) VALUES (@nomeContatto,@cognomeContatto,@numeroTelContatto)";
                comm.Parameters.AddWithValue("@nomeContatto",objContatto.Nome);
                comm.Parameters.AddWithValue("@cognomeContatto",objContatto.Cognome);
                comm.Parameters.AddWithValue("@numeroTelContatto",objContatto.numeroTel);

                connessione.Open();
                if (comm.ExecuteNonQuery() > 0)
                    return true;
            }
            return false;
        }
        public List<Contatto> GetListaContatto()
        {
            List<Contatto> elencoTemp = new List<Contatto>();

            using(SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                string query = "SELECT contattoId, nome,cognome,numeroTel FROM Contatto";
                SqlCommand comm = new SqlCommand(query, connessione);
                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Contatto temp = new Contatto()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Cognome = reader[2].ToString(),
                        numeroTel = reader[3].ToString()

                    };
                    elencoTemp.Add(temp);
                    
                    
                }
                return elencoTemp;
            }
        }
        public bool UpdateContatto(Contatto objContatto)
        {
            //dando vecchio e' nuovo contatto e' possibile fare un paragone e con una serie di if modificare solo i valori che serve modificare

            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "UPDATE Contatto SET nome = @varNome,Cognome = @varCognome,numeroTel = @varNumeroTel WHERE ContattoId = @varId";
                comm.Parameters.AddWithValue("@varNome", objContatto.Nome);
                comm.Parameters.AddWithValue("@varCognome", objContatto.Cognome);
                comm.Parameters.AddWithValue("@varNumeroTel", objContatto.numeroTel);
                comm.Parameters.AddWithValue("@variD", objContatto.Id);

                connessione.Open();
                int affRows = comm.ExecuteNonQuery();
                if (affRows > 0)
                    return true;
            }
            return false;
        }
        public bool DeleteContatto(int varId)
        {
            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                string query = "DELETE FROM Contatto WHERE ContattoId = @varId";
                SqlCommand comm = new SqlCommand(query,connessione);
                comm.Parameters.AddWithValue("@varId", varId);

                connessione.Open();
                if (comm.ExecuteNonQuery()>0){
                    return true;
                }
                return false;
            }
        }
        public bool GetContattoByNumeroTel(string varNumeroTel)
        {
            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                string query = "SELECT nome,cognome,numeroTel FROM Contatto WHERE numeroTel = @varNumeroTel";
                SqlCommand comm = new SqlCommand(query, connessione);
                comm.Parameters.AddWithValue("@VarNumeroTel", varNumeroTel);

                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();
                //si puo usare un reader.Read() con un try catch
                if (reader.HasRows)
                {
                   
                    return true;
                }
                else
                    return false;
            }
        } 
    }
}
