﻿using System;
using System.Collections.Generic;
using System.Text;

namespace taskGestionaleRubricaDB.Models
{
    public class Contatto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string numeroTel { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Nome} - {Cognome} - {numeroTel} ";
        }
    }

    
}
