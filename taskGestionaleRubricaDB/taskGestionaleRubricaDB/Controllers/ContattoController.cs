﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using taskGestionaleRubricaDB.DAL;
using taskGestionaleRubricaDB.Models;

namespace taskGestionaleRubricaDB.Controllers
{
    class ContattoController
    {   
        public static IConfiguration configurazione;    // divrebbe essere private

        public ContattoController()
        {
            //la config si potrebbe popolare con un if(configurazione==null)   ma il realoadOnChange dovrebbe diventare false
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
               

            configurazione = builder.Build();
        }

        public void InserisciContatto(string varNome,string varCognome,string varNumeroTel)
        {
            ContattoDAL contattoDal = new ContattoDAL(configurazione);


            if (contattoDal.GetContattoByNumeroTel(varNumeroTel)==false)
            {
                Contatto temp = new Contatto()
                {
                    Nome = varNome,
                    Cognome = varCognome,
                    numeroTel = varNumeroTel
                };

                if (contattoDal.InsertContatto(temp))
                {
                    Console.WriteLine("Contatto aggiunto");
                }
                else
                {
                    Console.WriteLine("Errore!!!");
                }
            }
            else
            {
                Console.WriteLine("Errore! Contatto gia presente in rubrica");
            }
        }
        public void stampaContatti()
        {
            ContattoDAL contattoDal = new ContattoDAL(configurazione);
            List<Contatto> tempList = contattoDal.GetListaContatto();

            foreach(Contatto temp in tempList)
            {
                Console.WriteLine(temp);
            }
            
        }

        public void modificaContattoPerId(int varId,string varNome,string varCognome,string varNumeroTel)
        {
            Contatto temp = new Contatto()
            {
                Id = varId,
                Nome = varNome,
                Cognome = varCognome,
                numeroTel = varNumeroTel
            };
            ContattoDAL contattoDal = new ContattoDAL(configurazione);

            if (contattoDal.UpdateContatto(temp))
            {
                Console.WriteLine("Contatto modificato con successo");
            }
            else
            {
                Console.WriteLine("Errore!!!");
            }

        }
        public void eliminaContattoPerId(int varId)
        {
            ContattoDAL contattoDal = new ContattoDAL(configurazione);

            if (contattoDal.DeleteContatto(varId))
            {
                Console.WriteLine("Ok!");
            }
            else
            {
                Console.WriteLine("Errore");
            }
        }


    }
}
