﻿using System;
using taskGestionaleRubricaDB.Controllers;

namespace taskGestionaleRubricaDB
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Creare un sistema di gestione di contatti, che memorizzi:
             * Nome
             * Cognome
             * Telefono (UNIVOCO)
             * 
             * - CRUD con DB
             * - Evitare l'inserimento di elementi già presenti in DB (evitare i metodi read generici)
             */
            // TODO Secondo campo nella traccia

            ContattoController gestore = new ContattoController();
            gestore.stampaContatti();
            gestore.InserisciContatto("capo","plaza","123123321");
            gestore.InserisciContatto("capo","plaza","123123321");
            gestore.eliminaContattoPerId(1);
            gestore.modificaContattoPerId(3,"senza","nome","444444444");

            gestore.stampaContatti();


        }
    }
}
