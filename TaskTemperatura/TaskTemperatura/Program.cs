﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTemperatura
{
    class Program
    {
        static void Main(string[] args)
        {
            //float temp = 30.0f;

            double temp = Convert.ToDouble(Console.ReadLine());

            if(temp>35f && temp < 42f)
            {
                if (temp <= 37.5f)
                {
                    Console.WriteLine("Accesso permesso");
                } else
                {
                    Console.WriteLine("Accesso negato");
                }
            }
            else
            {
                Console.WriteLine("Chiama il tuo medico");
            }



            //------------TEST INPUT
            //string variabile = Console.ReadLine();
            //Console.WriteLine($"La frase inserita e' {variabile}");
            //Console.ReadLine();

        }
    }
}
