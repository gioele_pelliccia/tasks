﻿using System;
using System.Collections.Generic;

namespace TaskGestionaleStudente
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
         * Realizzare un sistema di gestione dell'anagrafica studenti.
         * I valori da inserire, e consultare, sono i seguenti: Matricola, Nome, Città di provenienza, totale CFU conseguiti
         * tra 0 e 180 (0<cfu<60 - primo anno, 60< = cfu < 120 secondo anno, cfu > = 120 terzo anno )
         * Avviando l'applicazione, l'utente si troverà di fronte ad un menù a scelta, con le seguenti options:
         * 1. inserimento nuovo studente
         * 2. aggiornamento dati studente
         * 3. eliminazione studente
         * 4. visualizzazione dettaglio scheda studente
         * 5. visualizzazione lista studenti
         * 6. verifica risultati studente
         * 7. Uscita dall'applicazione
         */

            List<Student> studentList = new List<Student>();
            

            bool flag = true;

           




            while (flag)
            {
                Console.WriteLine("1. inserimento nuovo studente \n2. aggiornamento dati studente \n3. eliminazione studente\n4. visualizzazione dettaglio studente \n5. visualizzazione lista studenti\n6. verifica risultati studente\n7. Uscita dall'applicazione ");
                int input = Convert.ToInt32(Console.ReadLine());
                switch (input)
                {

                    case 1:
                        Console.WriteLine("Inserire Matricola:");
                        int matricola = Convert.ToInt32(Console.ReadLine());


                        if (studentList.Exists(Student => Student.Matricola == matricola) == true)
                        {
                            Console.WriteLine("La matricola immessa e' gia esistente \n");
                            break;
                        }
                        Console.WriteLine("Inserire nome");
                        String name = Console.ReadLine();
                        Console.WriteLine("Inserire citta");
                        String citta = Console.ReadLine();
                       
                            
                        
                        
                        Console.WriteLine("Inserire cfu");
                        int cfu = Convert.ToInt32(Console.ReadLine());
                        if (cfu < 0)
                        {
                            Console.WriteLine("Errore");
                            break;
                        }
                        studentList.Add(new Student { Nome = name, Citta = citta, Matricola = matricola, Cfu = cfu });
                        

                        break;

                    case 2:
                        Console.WriteLine("Inserire Matricola:");
                        int inputMatricola = Convert.ToInt32(Console.ReadLine());
                        foreach (Student student in studentList)
                        {
                            if (student.Matricola == inputMatricola)
                            {
                                Console.WriteLine("Inserire nuovi dati");
                                Console.WriteLine("Inserire nome");
                                student.Nome = Console.ReadLine();
                                Console.WriteLine("Inserire citta");
                                student.Citta = Console.ReadLine();
                                Console.WriteLine("Inserire cfu");
                                student.Cfu = Convert.ToInt32(Console.ReadLine());


                            }

                        }
                        break;

                    case 3:
                        Console.WriteLine("Inserire Matricola dello studente da eliminare");
                        int userInput = Convert.ToInt32(Console.ReadLine());
                        foreach (Student student in studentList)
                        {
                            if (student.Matricola == userInput)
                            {
                                studentList.Remove(student);        //soluzione errata

                            }
                        }
                        break;

                    case 4:
                        Console.WriteLine("Inserire Matricola dello studente da visualizzare");
                        int inputFour = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Inserire nome dello studente da visualizzare");
                        string inputName = Console.ReadLine();
                        foreach (Student student in studentList)
                        {
                            if (student.Matricola == inputFour && inputName.Equals(student.Nome))
                            {
                                Console.WriteLine($"{student.Nome}, {student.Citta}, Matricola: {student.Matricola} CFU: {student.Cfu}\n");

                            }
                            else
                            {
                                Console.WriteLine("le informazioni inserite sono errate");
                            }
                                

                        }
                        break;

                    case 5:
                        foreach (Student student in studentList)
                        {
                            Console.WriteLine(student.toString());
                        }
                        break;
                    case 6:
                        Console.WriteLine("Inserire Matricola dello studente da verificare");
                        int inputSix = Convert.ToInt32(Console.ReadLine());
                        foreach (Student student in studentList)
                        {
                            if (student.Matricola == inputSix)
                            {
                                if(student.Cfu<60 && student.Cfu > 0)
                                {
                                    student.Anno = "Primo anno";
                                }else if(student.Cfu >=60 && student.Cfu < 120)
                                {
                                    student.Anno = "Secondo anno";
                                } else if (student.Cfu >= 120)
                                {
                                    student.Anno = "Terzo anno";
                                }
                               
                                Console.WriteLine($"{student.Nome} ha {student.Cfu} Cfu ed e' al {student.Anno}\n");
                            }else
                            {
                                Console.WriteLine("Inserita matricola errata");
                            }
                        }
                            
                        break;

                    case 7:
                        flag = !flag;
                        break;
                    default:
                        Console.WriteLine("Comando non trovato");
                        break;
                }
            }
        }
        
    }
}

