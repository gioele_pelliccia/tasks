﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskGestionaleStudente
{
    class Student
    {
        /**
         * Realizzare un sistema di gestione dell'anagrafica studenti.
         * I valori da inserire, e consultare, sono i seguenti: Matricola, Nome, Città di provenienza, totale CFU conseguiti
         * tra 0 e 180 (0<cfu<60 - primo anno, 60< = cfu < 120 secondo anno, cfu > = 120 terzo anno )
         * Avviando l'applicazione, l'utente si troverà di fronte ad un menù a scelta, con le seguenti options:
         * 1. inserimento nuovo studente
         * 2. aggiornamento dati studente
         */

        public string Nome { get; set; }
        public string Citta { get; set; }
        public int Matricola { get; set; }
        public int Cfu {get;set;}

        public string Anno { get; set; }

        public Student()
        {

        }
        public string toString()
        {
            return $"{Nome}, {Citta}, Matricola: {Matricola} CFU: {Cfu}\n";
        }
        
    }
}
